/*
import javax.smartcardio.CardTerminal;
import java.util.concurrent.CountDownLatch;


//https://github.com/intarsys/smartcard-io
//https://www.win.tue.nl/pinpasjc/docs/apis/jc222/javacard/
public class SmartCardTerminal extends CardTerminal {

    class Listener implements INotificationListener {

        public CountDownLatch latch = new CountDownLatch(1);

        public boolean requirePresent;

        public Listener(boolean waitPresent) {
            super();
            this.requirePresent = waitPresent;
        }

        @Override
        public void handleEvent(Event event) {
            CardEvent ce = (CardEvent) event;
            synchronized (lockWait) {
                if (!ce.getNewState().isInvalid() == requirePresent) {
                    if (Log.isLoggable(Level.FINER)) {
                        Log.log(Level.FINER, CardTerminalImpl.this.getLabel()
                                + " found state change");
                    }
                    latch.countDown();
                }
            }
        }
    }

    private static final Logger Log = PACKAGE.Log;

    final private ICardTerminal cardTerminal;

    final private Object lockWait = new Object();

    protected CardTerminalImpl(ICardTerminal cardTerminal) {
        super();
        this.cardTerminal = cardTerminal;
    }

    @Override
    public Card connect(String protocolName) throws CardException {
        // protocol is one of "T=0", "T=1", "T=CL", "*"
        int protocol;
        if ("T=0".equals(protocolName)) {
            protocol = ICardTerminal.PROTOCOL_T0;
        } else if ("T=1".equals(protocolName)) {
            protocol = ICardTerminal.PROTOCOL_T1;
        } else if ("*".equals(protocolName)) {
            protocol = ICardTerminal.PROTOCOL_Tx;
        } else {
            throw new CardException("protocol " + protocolName
                    + " not supported");
        }
        ICard card = cardTerminal.getCard();
        if (card == null || card.getState().isInvalid()) {
            throw new CardNotPresentException("No card present");
        }
        Future<ICardConnection> f = card.connectShared(protocol, null);
        try {
            return createCard(f.get());
        } catch (InterruptedException e) {
            throw new CardException(e);
        } catch (ExecutionException e) {
            throw new CardException(e.getCause());
        }
    }

    protected Card createCard(ICardConnection connection) {
        return new CardImpl(connection);
    }

    public ICardTerminal getCardTerminal() {
        return cardTerminal;
    }

    public String getLabel() {
        return cardTerminal.toString();
    }

    @Override
    public String getName() {
        return cardTerminal.getName();
    }

    @Override
    public boolean isCardPresent() throws CardException {
        ICard card = cardTerminal.getCard();
        return card != null && !card.getState().isInvalid();
    }

    @Override
    public String toString() {
        return getLabel();
    }

    protected boolean waitForCard(long timeout, Listener listener) {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout must be >= 0" + timeout);
        }
        try {
            if (Log.isLoggable(Level.FINER)) {
                Log.log(Level.FINER, CardTerminalImpl.this.getLabel()
                        + " wait for card");
            }
            synchronized (lockWait) {
                // don't loose signal, synchronize
                cardTerminal.addNotificationListener(CardEvent.ID, listener);
                boolean present = cardTerminal.getCard() != null;
                if (present == listener.requirePresent) {
                    return true;
                }
            }
            if (timeout == 0) {
                listener.latch.await();
                return true;
            } else {
                return listener.latch.await(timeout, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            return false;
        } finally {
            cardTerminal.removeNotificationListener(CardEvent.ID, listener);
        }
    }

    @Override
    public boolean waitForCardAbsent(long timeout) throws CardException {
        return waitForCard(timeout, new Listener(false));
    }

    @Override
    public boolean waitForCardPresent(long timeout) throws CardException {
        return waitForCard(timeout, new Listener(true));
    }*/
