import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;

public class SmartCard extends Card {
    @Override
    public ATR getATR() {
        return null;
    }

    @Override
    public String getProtocol() {
        return null;
    }

    @Override
    public CardChannel getBasicChannel() {
        return null;
    }

    @Override
    public CardChannel openLogicalChannel() throws CardException {
        return null;
    }

    @Override
    public void beginExclusive() throws CardException {

    }

    @Override
    public void endExclusive() throws CardException {

    }

    @Override
    public byte[] transmitControlCommand(int i, byte[] bytes) throws CardException {
        return new byte[0];
    }

    @Override
    public void disconnect(boolean b) throws CardException {

    }
}
