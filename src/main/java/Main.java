import javax.smartcardio.*;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;


// sory for bad code
// i will change it for a day or to but all works and i give you little help in comemnts


public class Main {

    public static void main(String[] args) throws CardException, UnsupportedEncodingException {


        Card card = init(0);


            write(card, "Abrakadabra & Hello World");


           read(card);

    }




//Before using any card you need to initialize card
    static Card init(int DeviceSequenceNumber) throws CardException {

        CardTerminal terminal = getTerminal(DeviceSequenceNumber);

        Card card = terminal.connect("*");
        System.out.println("Card: " + card);
        CardChannel channel = card.getBasicChannel();
        byte [] data = {0x01, 0x06};

        ResponseAPDU r = channel.transmit(new CommandAPDU(0xff, 0xa4, 0x00, 0x00, data));
        String hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println(hex);


        return card;
    }


    // get terminal device to read card
    static CardTerminal getTerminal(int DeviceSequenceNumber) throws  CardException{
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = factory.terminals().list();
        System.out.println("Reader: " + terminals);
        CardTerminal terminal = terminals.get(DeviceSequenceNumber);
        return terminal;
    }

    //verify is card in cardreader
    static boolean isCard(CardTerminal terminal) throws CardException{
        return terminal.isCardPresent();
    }


    //read card memmory
    static String read(Card card) throws CardException {
        System.out.println("Card: " + card);
        CardChannel channel = card.getBasicChannel();


        // APDU for read card
        //first argument evrytime must be ff
        // second b0 - this is command to read
        // 3,4 arguments - MSB & LSB or memmory addreses
        // 5 argument - how much bytes you need to read

        ResponseAPDU r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0xff, 0x30, 0x10));
        String str = new String(r.getBytes(), StandardCharsets.UTF_8);
        System.out.println("Response: " + str);


        for (int i = 0; i < 16 ; i++) {

            r = channel.transmit(new CommandAPDU(0xff, 0xb0, ("0x0" + Integer.toHexString(i)).hashCode(), 0x00, 0x10));
            str = new String(r.getBytes(), StandardCharsets.UTF_8);
            System.out.println("Response: " + str);

            System.out.println();
        }



        //Commented block read all memmory without security
      /*  r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x00, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);


        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x10, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x20, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x30, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x40, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x50, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x60, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x70, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x80, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0x90, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xA0, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xB0, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xC0, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xD0, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);

         r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xE0, 0x10));
         hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);
        r = channel.transmit(new CommandAPDU(0xff, 0xb0, 0x00, 0xF0, 0x10));
        hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);*/


        return new String();

    }


    // method to write to smartcard
    static void write(Card card, String s ) throws UnsupportedEncodingException, CardException {

        System.out.println("Card: " + card);
        CardChannel channel = card.getBasicChannel();
        byte [] data = s.getBytes();

        // APDU for read card
        //first argument evrytime must be ff
        // second d0 - this is command to write
        // 3,4 arguments - MSB & LSB or memmory addreses
        // 5 argument - how much bytes you need to write
        // another arguments - data to write

        ResponseAPDU r = channel.transmit(new CommandAPDU(0xff, 0xd0, 0x01, 0x30, data));
        String hex = DatatypeConverter.printHexBinary(r.getBytes());
        System.out.println("Response: " + hex);


    }

}
